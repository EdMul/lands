﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Lands
{
    using Xamarin.Forms;
    using Views;

    public partial class App : Application
    {


        #region constructors
        public App()
        {
            InitializeComponent();

            /*---------------------------------------
             *POR ACA EMPIEZA TODO 
             -----------------------------------------*/

            MainPage = new NavigationPage(new LoginPage());
            MainPage.Title = "Login";
            
        }

        #endregion

        #region Events
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.Models
{
    using Newtonsoft.Json;

    public class RegionalBloc
    {
    #region Properties
        
    [JsonProperty(PropertyName = "acronym")]
    public string acronym { get; set; }

    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }
                                                                                                       
        
    // Ignorada: public List<object> otherAcronyms { get; set; }
    // Ignorada: public List<object> otherNames { get; set; }


    #endregion
    }
}

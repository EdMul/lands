﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.Models
{
    //Es la respuesta que dan los servicios
    //para saber si respondieron exitosamente a un requerimiento
    public class Response
    {
        public bool IsSuccess
        {
            get;
            set;
        }

        // Mensaje de Error si IsSuccess = false
        public string Message
        {
            get;
            set;
        }
        // objeto o colección si IsSuccess = true
        public object Result
        {
            get;
            set;
        }

    }
}

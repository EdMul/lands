﻿namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;  //Para simplificar los eventos?? no recuerdo
    using System.Collections.ObjectModel;
    using Services;
    using Models;
    using Xamarin.Forms;
    using System.Collections.Generic;
    using System.Windows.Input;
    using Lands.Views;


    //se crean para los controlos que se deben refrescar en pantalla desde el código c#
    //queda entonces:  (ej: Password)
    //el control en pantalla: Password
    //la propiedad pública en c# Password (this.Password)
    //el atriburo privado: password  en minúscula 

    //y en el caso de los botones o lo que necesite un evento para discaparce en el código ante una acción de usuarip
    //Login en pantalla
    //LoginComand evento
    //Login acción
    //llama a un evento asincrónico Login

    public class LogicViewModel : BaseViewModel

    {

        #region Atributes
        private string email;
        private string password;
        private bool isRunning;
        private bool isEnabled;
        #endregion

        #region Constructors
        public LogicViewModel()
        {
            
            this.IsEnabled = true;            
            this.IsRemembered = true;

            this.Email = "edmultare@gmail.com";
            this.Password = "1234";
            // https://restcountries.eu/rest/v2/all
        }
        #endregion

        #region Properties
        public string Email
        {
            get { return this.email; }
            set { SetValue(ref email, value); }
        }

        public string Password
        {
            get {return this.password; }
            set { SetValue(ref password, value); }
        }


        public bool IsRunning
        {
            get {return this.isRunning;}
            set {SetValue(ref isRunning, value);}
        }
        public bool IsRemembered
        {
            get;
            set;
        }


        public bool IsEnabled
        {
            get {return this.isEnabled; }
            set { SetValue(ref isEnabled, value); }
        }


        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }

        }


        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an email.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an password.",
                    "Accept");
                return;
            }


            this.IsEnabled = true;  //esto no se ve x q pasa super rápdido
            this.isRunning = false; //esto no se ve x q pasa super rápdido 


            // Acá se llamará al servicio para ver si está bien el login...
            // por ahora lo hago fijo

            if (this.Email != "edmultare@gmail.com" || this.Password != "1234")   
            {                                                           
                this.IsEnabled = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Email or password incorrect.",
                    "Accept");
                this.Password = string.Empty;
                return;
            }

            
            this.isRunning= true;
            this.IsEnabled = false;


            // Ya verifiqué que está bien, entonces borro las propiedades para que no quede la info en la propiedad.
            this.Email = string.Empty;
            this.Password = string.Empty;

            // Ahora se navega a la página correcta. Son métodos asyncronos. 
            // Navegar a la próxima página: se apila = PUSH
            // Navegar a la anteriro página: se desapila = POP

            // Ppara instanciar la LandsPage se instancia la MainViewModel, para evitar crear muchas instancias es uq
            // se crea una Singleton instance

            MainViewModel.GetInstance().Lands = new LandsViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());


            this.isRunning = false;
            this.IsEnabled = true;  //Se prenden los botones para q lo encuentra prendido si vuelve


        }
        #endregion
    }
}
                                                                                           
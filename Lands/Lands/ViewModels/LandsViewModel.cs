﻿
namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;  //Para simplificar los eventos?? no recuerdo
    using System.Collections.ObjectModel;
    using Services;
    using Models;
    using Xamarin.Forms;
    using System.Collections.Generic;
    using System.Windows.Input;
    using System;
    using System.Linq;

    public class LandsViewModel : BaseViewModel

    {
        #region Services
        private ApiService apiService;
        

        #endregion

        #region Attributes
        private ObservableCollection<LandItemViewModel> lands;
        private bool isRefreshing;
        private string filter;
        private List<Land> landsList;
        #endregion

        #region Properties

        // Es observablecollection porque debe refrescar controls en teste caso ListView
        public ObservableCollection<LandItemViewModel> Lands
        {
            get { return this.lands; }
            set { SetValue(ref this.lands, value); }
            
        }

        public string Filter
        {
            get { return this.filter; }
            set { 
                    SetValue(ref this.filter, value);
                    this.Search();  // Poniendo acá que busque hará la busqueda con cada tecla que pulse el usuario.
                }

            }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value);}

        }
        #endregion

        #region Constructors
        public LandsViewModel()
        {
            this.apiService = new ApiService();
            this.LoadLands();

        }

    
        private async void LoadLands()
        {
            // Verificar si hay conexión
            this.IsRefreshing = true;
            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Accept");
                await Application.Current.MainPage.Navigation.PopAsync(); //como si hubiese apretado la flecha pa atrás del celu.
                return; 
                                       
            }

            // Traer lista de países y castearla a ObservableCollection para poder bindearla a controles
            var response =  await this.apiService.GetList<Land>("https://restcountries.eu", "/rest", "/v2/all");

            if (! response.IsSuccess) 
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message, 
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync(); //como si hubiese apretado la flecha pa atrás del celu.
                return;
            }
            this.IsRefreshing = false;

            //Si lo hago así se ejecuta una vez bien pero luego la lista se pierde porque es una var
            //var list = (List<Land>)response.Result;   

            //En cambio si lo hago en un atributo privado del objeto no se pierde 
            this.landsList  = (List<Land>)response.Result;
            this.Lands = new ObservableCollection<LandItemViewModel>(this.ToLandViewModel());
        }

        #endregion

        #region Methods


      

        private IEnumerable<LandItemViewModel> ToLandViewModel()

        {
            return this.landsList.Select(l => new LandItemViewModel
            {
                Alpha2Code = l.Alpha2Code,
                Alpha3Code = l.Alpha3Code,
                AltSpellings = l.AltSpellings,
                Area = l.Area,
                Borders = l.Borders,
                CallingCodes = l.CallingCodes,
                Capital = l.Capital,
                Cioc = l.Cioc, 
                Currencies = l.Currencies, 
                Demonym = l.Demonym, 
                Flag = l.Flag, 
                Gini = l.Gini, 
                Languages = l.Languages, 
                Latlng = l.Latlng, 
                Name = l.Name, 
                NativeName = l.NativeName, 
                NumericCode = l.NumericCode, 
                Population = l.Population, 
                Region = l.Region, 
                RegionalBlocs = l.RegionalBlocs, 
                Subregion = l.Subregion, 
                Timezones  = l.Timezones, 
                TopLevelDomain = l.TopLevelDomain, 
                Translations = l.Translations,
            });

        }



        #endregion
                                                    
        #region Commands
      
        public ICommand RefreshingCommand
        {
            get { return new RelayCommand(LoadLands); }

        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(Search); }
            
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))                                                    
            {
                this.Lands = new ObservableCollection<LandItemViewModel>(this.ToLandViewModel());

            }
            else
            {
                this.Lands = new ObservableCollection<LandItemViewModel>(
                    this.ToLandViewModel().Where(l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                                              l.Capital.ToLower().Contains(this.Filter.ToLower())));

            }
        }
                                                          
        #endregion



    }
}                                            
                         
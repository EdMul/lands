﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Lands.Views;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Models;
    public class LandItemViewModel : Land
    {

        public ICommand SelectLandCommand
        {
            get
            {
                return new RelayCommand(SelectCommand);
            }                                                              

        }

        private async void SelectCommand()
        {
            MainViewModel.GetInstance().Land = new LandViewModel(this);
            await Application.Current.MainPage.Navigation.PushAsync(new LandTabbedPage());
        }
    }                                                             
}
                                             
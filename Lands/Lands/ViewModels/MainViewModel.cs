﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.ViewModels
{
    class MainViewModel
    {
        #region ViewModels
        public LogicViewModel Login 
        { 
            get; 
            set; 
        }                                                                                    

        public LandsViewModel Lands
        {
            get;
            set;
        }

        public LandViewModel Land
        {
            get;
            set;
        }

        #endregion

        #region Constructors                                         
        public MainViewModel()
        {
            instance = this;
            this.Login = new  LogicViewModel();
        }
        #endregion                                   


        #region Singleton

        private static MainViewModel instance;

        public static MainViewModel GetInstance()
                                  
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;                                

        }

                                   

        #endregion

    }
}
